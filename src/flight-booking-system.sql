CREATE TABLE Airport (
  code      char(3) NOT NULL,
  name      varchar(255) NOT NULL,
  city      varchar(255) NOT NULL,
  country   varchar(255) NOT NULL,
  time_zone varchar(64) NOT NULL,
  PRIMARY KEY (code)) ENGINE=InnoDB;
CREATE TABLE Flight (
  id                int(10) NOT NULL AUTO_INCREMENT,
  departure_time    datetime NOT NULL,
  arrival_time      datetime NOT NULL,
  departure_airport char(3) NOT NULL,
  arrival_airport   char(3) NOT NULL,
  plane             int(10) NOT NULL,
  price             decimal(6, 2) NOT NULL,
  PRIMARY KEY (id)) ENGINE=InnoDB;
CREATE TABLE Plane (
  id       int(10) NOT NULL AUTO_INCREMENT,
  model    varchar(255) NOT NULL,
  seats_nb int(10) NOT NULL,
  PRIMARY KEY (id)) ENGINE=InnoDB;
CREATE TABLE Customer (
  id         int(11) NOT NULL AUTO_INCREMENT,
  email      varchar(255) NOT NULL UNIQUE,
  password   varchar(255) NOT NULL,
  first_name varchar(255) NOT NULL,
  last_name  varchar(255) NOT NULL,
  PRIMARY KEY (id)) ENGINE=InnoDB;
CREATE TABLE Ticket (
  id                   int(11) NOT NULL AUTO_INCREMENT,
  customer             int(11) NOT NULL,
  flight               int(10) NOT NULL,
  passenger_first_name varchar(255) NOT NULL,
  passenger_last_name  varchar(255) NOT NULL,
  luggage              int(11) NOT NULL,
  PRIMARY KEY (id)) ENGINE=InnoDB;
CREATE TABLE LuggageAllowance (
  weight      int(11) NOT NULL,
  price_ratio decimal(3, 2) NOT NULL,
  PRIMARY KEY (weight)) ENGINE=InnoDB;
CREATE TABLE Admin (
  login    varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  PRIMARY KEY (login)) ENGINE=InnoDB;
ALTER TABLE Flight ADD CONSTRAINT `departs from` FOREIGN KEY (departure_airport) REFERENCES Airport (code);
ALTER TABLE Flight ADD CONSTRAINT `arrives at` FOREIGN KEY (arrival_airport) REFERENCES Airport (code);
ALTER TABLE Flight ADD CONSTRAINT `is done with` FOREIGN KEY (plane) REFERENCES Plane (id);
ALTER TABLE Ticket ADD CONSTRAINT `has been bought by` FOREIGN KEY (customer) REFERENCES Customer (id);
ALTER TABLE Ticket ADD CONSTRAINT `allows to take` FOREIGN KEY (flight) REFERENCES Flight (id);
ALTER TABLE Ticket ADD CONSTRAINT allows FOREIGN KEY (luggage) REFERENCES LuggageAllowance (weight);
INSERT INTO Airport(code, name, city, country, time_zone) VALUES ('LYS', 'Lyon St Exupery', 'Lyon', 'France', 'Europe/Paris');
INSERT INTO Airport(code, name, city, country, time_zone) VALUES ('CDG', 'Charles de Gaules', 'Paris', 'France', 'Europe/Paris');
INSERT INTO Airport(code, name, city, country, time_zone) VALUES ('DUB', 'Dublin International Airport', 'Dublin', 'Ireland', 'Europe/Dublin');
INSERT INTO Airport(code, name, city, country, time_zone) VALUES ('ORK', 'Cork Airport', 'Cork', 'Ireland', 'Europe/Dublin');
INSERT INTO Plane(id, model, seats_nb) VALUES (1, 'Airbus A320', 80);
INSERT INTO Flight(id, departure_time, arrival_time, departure_airport, arrival_airport, plane, price) VALUES (1, '2019-11-20 12:00', '2019-11-20 13:30', 'LYS', 'DUB', 1, 70);
INSERT INTO Flight(id, departure_time, arrival_time, departure_airport, arrival_airport, plane, price) VALUES (2, '2019-11-30 22:00', '2019-11-30 23:30', 'CDG', 'ORK', 1, 50);
INSERT INTO Customer(id, email, password, first_name, last_name) VALUES (1, 'fabien.sailliet@mycit.ie', 'password', 'Fabien', 'Sailliet');
INSERT INTO LuggageAllowance(weight, price_ratio) VALUES (20, 0.5);
INSERT INTO LuggageAllowance(weight, price_ratio) VALUES (10, 0.3);
INSERT INTO LuggageAllowance(weight, price_ratio) VALUES (0, 0);
INSERT INTO Admin(login, password) VALUES ('admin', 'admin');
