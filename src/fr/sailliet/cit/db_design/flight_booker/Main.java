package fr.sailliet.cit.db_design.flight_booker;

import fr.sailliet.cit.db_design.flight_booker.model.*;
import fr.sailliet.cit.db_design.flight_booker.modules.*;

import java.sql.Connection;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            Connection connection = ConnectionFactory.getConnection();

            DAOManager daoManager = new DAOManager(connection);

            ApplicationModulesManager modulesManager = new ApplicationModulesManager();
            modulesManager.setFlightsModule(new FlightsModule(sc, daoManager, modulesManager));
            modulesManager.setCustomerModule(new CustomerModule(sc, daoManager, modulesManager));
            modulesManager.setAirportsModule(new AirportsModule(sc, daoManager, modulesManager));
            modulesManager.setAdminModule(new AdminModule(sc, daoManager, modulesManager));
            modulesManager.setPlanesModule(new PlanesModule(sc, daoManager, modulesManager));

            new MenusModule(sc, daoManager, modulesManager).mainMenu();
        }
    }
}
