package fr.sailliet.cit.db_design.flight_booker;

import fr.sailliet.cit.db_design.flight_booker.model.*;

import java.sql.Connection;

public class DAOTests {

    public static void main(String[] args) {
        Connection connection = ConnectionFactory.getConnection();

        AirportDAO airportDAO = new AirportDAO(connection);
        CustomerDAO customerDAO = new CustomerDAO(connection);
        FlightDAO flightDAO = new FlightDAO(connection);
        LuggageAllowanceDAO luggageAllowanceDAO = new LuggageAllowanceDAO(connection);
        PlaneDAO planeDAO = new PlaneDAO(connection);
        TicketDAO ticketDAO = new TicketDAO(connection);



        System.out.println("---------- Get all tests ----------\n");

        System.out.println("Airports:");
        for (Airport airport: airportDAO.getAll()) {
            System.out.println("\t- " + airport);
        }
        System.out.println();

        System.out.println("Customers:");
        for (Customer customer: customerDAO.getAll()) {
            System.out.println("\t- " + customer);
        }
        System.out.println();

        System.out.println("Flights:");
        for (Flight flight: flightDAO.getAll()) {
            System.out.println("\t- " + flight);
        }
        System.out.println();

        System.out.println("Luggage allowances:");
        for (LuggageAllowance allowance: luggageAllowanceDAO.getAll()) {
            System.out.println("\t- " + allowance);
        }
        System.out.println();

        System.out.println("Planes:");
        for (Plane plane: planeDAO.getAll()) {
            System.out.println("\t- " + plane);
        }
        System.out.println();

        System.out.println("Tickets:");
        for (Ticket ticket: ticketDAO.getAll()) {
            System.out.println("\t- " + ticket);
        }
        System.out.println();



        System.out.println("---------- Get one tests ----------\n");

        try {
            System.out.println("Customer with id 1: " + customerDAO.get(1));
        } catch (CannotFindDataException e) {
            System.err.println("Cannot find customer with id 1");
        }
        System.out.println();

        try {
            System.out.println("Airport with code LYS: " + airportDAO.get("LYS"));
        } catch (CannotFindDataException e) {
            System.err.println("Cannot find airport with code LYS");
        }
        System.out.println();

        try {
            System.out.println("Flight with id 1: " + flightDAO.get(1));
        } catch (CannotFindDataException e) {
            System.err.println("Cannot find flight with id 1");
        }
        System.out.println();

        try {
            System.out.println("Luggage allowance with weight 10: " + luggageAllowanceDAO.get(10));
        } catch (CannotFindDataException e) {
            System.err.println("Cannot find luggage allowance with weight 10");
        }
        System.out.println();

        try {
            System.out.println("Plane with id 1: " + planeDAO.get(1));
        } catch (CannotFindDataException e) {
            System.err.println("Cannot find plane with id 1");
        }
        System.out.println();

        try {
            System.out.println("Ticket with id 1: " + ticketDAO.get(1));
        } catch (CannotFindDataException e) {
            System.err.println("Cannot find ticket with id 1");
        }
        System.out.println();
    }
}
