package fr.sailliet.cit.db_design.flight_booker.modules;

public class ApplicationModulesManager {

    private FlightsModule flightsModule;
    private CustomerModule customerModule;
    private AirportsModule airportsModule;
    private AdminModule adminModule;
    private PlanesModule planesModule;



    // Getters

    public FlightsModule getFlightsModule() {
        return this.flightsModule;
    }

    public CustomerModule getCustomerModule() {
        return this.customerModule;
    }

    public AirportsModule getAirportsModule() {
        return this.airportsModule;
    }

    public AdminModule getAdminModule() {
        return this.adminModule;
    }

    public PlanesModule getPlanesModule() {
        return this.planesModule;
    }



    // Setters

    public void setFlightsModule(FlightsModule flightsModule) {
        this.flightsModule = flightsModule;
    }

    public void setCustomerModule(CustomerModule customerModule) {
        this.customerModule = customerModule;
    }

    public void setAirportsModule(AirportsModule airportsModule) {
        this.airportsModule = airportsModule;
    }

    public void setAdminModule(AdminModule adminModule) {
        this.adminModule = adminModule;
    }

    public void setPlanesModule(PlanesModule planesModule) {
        this.planesModule = planesModule;
    }
}
