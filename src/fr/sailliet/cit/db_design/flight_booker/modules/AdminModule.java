package fr.sailliet.cit.db_design.flight_booker.modules;

import fr.sailliet.cit.db_design.flight_booker.model.Admin;
import fr.sailliet.cit.db_design.flight_booker.model.CannotFindDataException;
import fr.sailliet.cit.db_design.flight_booker.model.DAOManager;

import java.util.Scanner;

public class AdminModule extends ApplicationModule {

    // Constructors

    public AdminModule(Scanner scanner, DAOManager daoManager, ApplicationModulesManager modulesManager) {
        super(scanner, daoManager, modulesManager);
    }



    // Module methods

    /**
     * Ask an admin login/password pair.
     *
     * @throws BackSelectedException The user selected back
     */
    public void logIn() throws BackSelectedException {
        this.displayHeader("Log in");

        Admin admin;

        // Get admin
        while (true) {
            String login = this.notEmptyStringInput("Login");

            try {
                admin = this.getDaoManager().getAdminDAO().get(login);
                break;
            } catch (CannotFindDataException e) {
                System.out.println("\nInvalid login");
            }
        }

        // Check admin password
        while (true) {
            String password = this.notEmptyStringInput("Password");

            if (password.equals(admin.getPassword())) {
                break;
            }
            else {
                System.out.println("\nInvalid password");
            }
        }

        System.out.println(String.format(
            "\nLogged in as: %s",
            admin.getLogin()
        ));
    }
}
