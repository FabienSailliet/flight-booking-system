package fr.sailliet.cit.db_design.flight_booker.modules;

/**
 * Element displayed in an action menu
 */
public class MenuElement {
    private String label;
    private MenuCallback callback;

    /**
     * Create a new menu element.
     *
     * @param label Text to be displayed in the menu
     * @param callback Action to perform if selected
     */
    public MenuElement(String label, MenuCallback callback) {
        this.label = label;
        this.callback = callback;
    }

    public String getLabel() {
        return this.label;
    }

    public MenuCallback getCallback() {
        return this.callback;
    }
}
