package fr.sailliet.cit.db_design.flight_booker.modules;

import fr.sailliet.cit.db_design.flight_booker.model.CannotFindDataException;
import fr.sailliet.cit.db_design.flight_booker.model.Customer;
import fr.sailliet.cit.db_design.flight_booker.model.DAOManager;

import java.util.Arrays;
import java.util.Scanner;

public class CustomerModule extends ApplicationModule {

    private Customer connectedCustomer;



    // Constructors

    public CustomerModule(Scanner scanner, DAOManager daoManager, ApplicationModulesManager modulesManager) {
        super(scanner, daoManager, modulesManager);
    }



    // Getters

    /**
     * Get the currently connected user. If not, ask the user to log in or sign in, and then return it.
     *
     * @return The currently connected user
     * @throws BackSelectedException The user selected back
     */
    public Customer getConnectedCustomer() throws BackSelectedException {
        if (this.connectedCustomer == null) {
            this.logInOrSignIn();
            return this.connectedCustomer;
        }

        return this.connectedCustomer;
    }

    /**
     * Indicate if a user is currently connected.
     *
     * @return True if a user is currently connected, false else
     */
    public boolean isACustomerConnected() {
        return (this.connectedCustomer != null);
    }



    // Module methods

    /**
     * Ask the user if it want to log into an existing customer account or create a new one, then run the appropriate action.
     *
     * @throws BackSelectedException The user selected back
     */
    public void logInOrSignIn() throws BackSelectedException {
        this.askSelectInList(
            "Use an existing account or create a new one?",
            Arrays.asList(
                new MenuElement("Log in", this::logIn),
                new MenuElement("Sign in", this::signIn)
            ),
            MenuElement::getLabel
        ).getCallback().run();
    }

    /**
     * Ask to the user login information, until correct or back selected. If correct, update the currently connected user.
     *
     * @throws BackSelectedException The user selected back
     */
    public void logIn() throws BackSelectedException {
        this.displayHeader("Log in");

        try {
            Customer customer;

            // Get customer with email
            while (true) {
                String email = this.notEmptyStringInput("Email address");

                try {
                    customer = this.getDaoManager().getCustomerDAO().getByEmail(email);
                    break;
                } catch (CannotFindDataException e) {
                    System.out.println("\nNo user is associated with this email");
                }
            }

            // Check customer password
            while (true) {
                String password = this.notEmptyStringInput("Password");

                if (password.equals(customer.getPassword())) {
                    break;
                }
                else {
                    System.out.println("\nInvalid password");
                }
            }

            System.out.println(String.format(
                "\nLogged in as: %s %s",
                customer.getFirstName(), customer.getLastName()
            ));

            this.connectedCustomer = customer;
        }
        finally {
            this.displayFooter();
        }
    }

    /**
     * Log out the currently connected user.
     */
    public void logOut() {
        this.connectedCustomer = null;
        System.out.println("\nYou have been disconnected");
    }

    /**
     * Create a new customer account by asking the user for information, then update the currently connected user.
     *
     * @throws BackSelectedException The user selected back
     */
    public void signIn() throws BackSelectedException {
        while (true) {
            this.displayHeader("Sign in");

            String firstName = this.notEmptyStringInput("First name");
            try {
                String lastName = this.notEmptyStringInput("Last name");

                String email;
                while (true) {
                    email = this.notEmptyStringInput("Email address");
                    if (this.getDaoManager().getCustomerDAO().isEmailFree(email)) {
                        break;
                    }
                    System.out.println("This email address is already taken, please input another one");
                }

                String password = this.notEmptyStringInput("Password");

                Customer customer = new Customer(
                    -1,
                    email,
                    password,
                    firstName,
                    lastName
                );

                this.getDaoManager().getCustomerDAO().insertWithAutoIncrement(customer);

                System.out.println("\nYou account has been created");

                try {
                    this.connectedCustomer = this.getDaoManager().getCustomerDAO().getByEmail(customer.getEmail());
                } catch (CannotFindDataException e) {
                    this.connectedCustomer = null;
                    System.out.println("\nError while trying to connect to the newly created account, try to connect manually");
                }

                return;
            }
            catch (BackSelectedException e) {
                // Repeat
            }
        }
    }
}
