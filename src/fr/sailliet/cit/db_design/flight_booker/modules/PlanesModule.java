package fr.sailliet.cit.db_design.flight_booker.modules;

import fr.sailliet.cit.db_design.flight_booker.model.DAOManager;
import fr.sailliet.cit.db_design.flight_booker.model.Plane;

import java.util.Scanner;

public class PlanesModule extends ApplicationModule {

    // Constructors

    public PlanesModule(Scanner scanner, DAOManager daoManager, ApplicationModulesManager modulesManager) {
        super(scanner, daoManager, modulesManager);
    }



    // Module methods

    /**
     * Ask the user to select a plane.
     *
     * @param prompt Text to display before the planes list
     * @return The selected plane
     * @throws BackSelectedException The user selected back
     */
    public Plane selectPlane(String prompt) throws BackSelectedException {
        return this.askSelectInList(
            prompt,
            this.getDaoManager().getPlaneDAO().getAll(),
            plane -> String.format("%s, %s, %s seats", plane.getId(), plane.getModel(), plane.getSeatsNb())
        );
    }

    /**
     * Ask the user information to create a plane, then create it and add it to the database.
     *
     * @throws BackSelectedException The user selected back
     */
    public void createPlane() throws BackSelectedException {
        while (true) {
            this.displayHeader("Plane creation");

            String model = this.notEmptyStringInput("Model");
            try {
                int seatNb = this.positiveIntInput("Number of seats");

                Plane plane = new Plane(-1, model, seatNb);

                this.getDaoManager().getPlaneDAO().insertWithAutoIncrement(plane);

                System.out.println("\nThe plane has been added");

                return;
            }
            catch (BackSelectedException e) {
                // Repeat
            }
        }
    }
}
