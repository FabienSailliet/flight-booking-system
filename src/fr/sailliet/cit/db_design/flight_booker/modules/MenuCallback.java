package fr.sailliet.cit.db_design.flight_booker.modules;

/**
 * Callback that is part of a menu.
 */
public interface MenuCallback {
    /**
     * Run the callback.
     *
     * @throws BackSelectedException The user ask to go back
     */
    void run() throws BackSelectedException;
}
