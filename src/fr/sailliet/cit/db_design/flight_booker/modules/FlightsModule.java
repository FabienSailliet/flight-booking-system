package fr.sailliet.cit.db_design.flight_booker.modules;

import fr.sailliet.cit.db_design.flight_booker.model.*;

import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Scanner;

public class FlightsModule extends ApplicationModule {

    // Constructors

    public FlightsModule(Scanner scanner, DAOManager daoManager, ApplicationModulesManager modulesManager) {
        super(scanner, daoManager, modulesManager);
    }



    // Module methods

    /**
     * Ask the user to pick a flight and then ask information to book it.
     *
     * @throws BackSelectedException The user selected back
     */
    public void searchAndBookAFlight() throws BackSelectedException {
        Flight desiredFlight = this.searchFlightByAirportsAndTime();

        this.bookFlight(desiredFlight);
    }

    /**
     * Ask the user to pick a flight by asking departure and arrival airports and desired departure date.
     *
     * @return The selected flight
     * @throws BackSelectedException The user selected back
     */
    public Flight searchFlightByAirportsAndTime() throws BackSelectedException {
        this.displayHeader("Flight Search");

        // Airports selection

        Airport departureAirport = this.getModulesManager().getAirportsModule().selectAirport("Departure airport");
        Airport arrivalAirport = this.getModulesManager().getAirportsModule().selectAirport("Arrival airport");

        // Select departure date

        List<Flight> flights;
        while (true) {
            LocalDate expDepartureDate = this.dateInput("Desired departure date");

            // Flights display and selection

            flights = this.getDaoManager().getFlightDAO().findBetweenTwoAirports(
                departureAirport, arrivalAirport, expDepartureDate
            );

            if (flights.size() > 0) {
                break;
            }

            System.out.println("\nNo flight has been found with this parameters, you can try another date");
        }

        // Select flight

        return this.askSelectInList(
            "Available flights (displayed with local times)",
            flights,
            flight -> String.format(
                "%s - %s\nBase price: %s",
                this.formatDateTime(departureAirport.getLocalTime(flight.getDepartureTime())),
                this.formatDateTime(arrivalAirport.getLocalTime(flight.getArrivalTime())),
                this.formatPrice(flight.getPrice())
            )
        );
    }

    /**
     * Ask the user information to book a given flight.
     *
     * @param flight The flight to book
     * @throws BackSelectedException The user selected back
     */
    public void bookFlight(Flight flight) throws BackSelectedException {
        while (true) {
            this.displayHeader("Flight Booking");

            System.out.println("\nPlease input the passenger details");
            String passengerFirstName = this.notEmptyStringInput("First name");

            try {
                String passengerLastName = this.notEmptyStringInput("Last name");

                LuggageAllowance luggage = this.askSelectInList(
                    "Luggage allowance",
                    this.getDaoManager().getLuggageAllowanceDAO().getAllSortedByWeight(),
                    luggageAllowance -> String.format(
                        "%skg - %s",
                        luggageAllowance.getWeight(),
                        this.formatPrice(luggageAllowance.getPriceRatio() * flight.getPrice())
                    )
                );

                Customer customer = this.getModulesManager().getCustomerModule().getConnectedCustomer();

                Ticket ticket = new Ticket(
                    -1,
                    customer.getId(),
                    flight.getId(),
                    passengerFirstName,
                    passengerLastName,
                    luggage.getWeight()
                );

                this.askConfirmation(String.format(
                    "Do you want to confirm this booking of %s?",
                    this.formatPrice(flight.getPrice() * (1 + luggage.getPriceRatio()))
                ));

                this.getDaoManager().getTicketDAO().insertWithAutoIncrement(ticket);

                System.out.println("\nThe booking has been registered");

                return;
            }
            catch (BackSelectedException e) {
                // Repeat
            }
        }
    }

    /**
     * Display to a connected customer all its bookings, then ask to cancel it.
     *
     * @throws BackSelectedException The user selected back
     */
    public void manageBookings() throws BackSelectedException {
        this.displayHeader("Bookings Management");

        Customer customer = this.getModulesManager().getCustomerModule().getConnectedCustomer();

        List<Ticket> bookings = this.getDaoManager().getTicketDAO().getAllByCustomer(customer);

        if (bookings.size() == 0) {
            System.out.println("\nYou have no booking");
            return;
        }

        Ticket selectedBooking = this.askSelectInList(
            "Choose a booking to cancel",
            bookings,
            ticket -> {
                Flight flight;
                Airport departureAirport, arrivalAirport;
                try {
                    flight = this.getDaoManager().getFlightDAO().get(ticket.getFlight());
                    departureAirport = this.getDaoManager().getAirportDAO().get(flight.getDepartureAirport());
                    arrivalAirport = this.getDaoManager().getAirportDAO().get(flight.getArrivalAirport());
                } catch (CannotFindDataException e) {
                    return String.format(
                        "Cannot get informations about booking %s",
                        ticket.getId()
                    );
                }

                String flightStr = String.format(
                    "%s %s - %s %s",
                    departureAirport.getCode(),
                    this.formatDateTime(departureAirport.getLocalTime(flight.getDepartureTime())),
                    arrivalAirport.getCode(),
                    this.formatDateTime(arrivalAirport.getLocalTime(flight.getArrivalTime()))
                );

                return String.format(
                    "%s\n%s %s\nLuggage allowance: %skg",
                    flightStr,
                    ticket.getPassengerFirstName(), ticket.getPassengerLastName(),
                    ticket.getLuggage()
                );
            }
        );

        this.askConfirmation("Do you really want to cancel this booking?");

        this.getDaoManager().getTicketDAO().delete(selectedBooking);
    }

    /**
     * Ask the user information to create a new flight, and then add it to the database.
     *
     * @throws BackSelectedException The user selected back
     */
    public void createFlight() throws BackSelectedException {
        while (true) {
            this.displayHeader("Flight creation");

            Airport departureAirport = this.getModulesManager().getAirportsModule()
                .selectAirport("Departure airport");
            try {
                Airport arrivalAirport = this.getModulesManager().getAirportsModule()
                    .selectAirport("Arrival airport");

                ZonedDateTime departureTime = this.dateTimeInput("Departure time")
                    .atZone(ZoneId.of(departureAirport.getTimeZone()));
                Duration flightDuration = this.durationInput("Flight duration");

                ZonedDateTime arrivalTime = departureTime.plus(flightDuration);

                Plane plane = this.getModulesManager().getPlanesModule().selectPlane("Plane");
                double price = this.doubleInput("Price");

                Flight flight = new Flight(
                    -1,
                    departureTime,
                    arrivalTime,
                    departureAirport.getCode(),
                    arrivalAirport.getCode(),
                    plane.getId(),
                    price
                );

                this.getDaoManager().getFlightDAO().insertWithAutoIncrement(flight);

                System.out.println("\nThe flight has been added");

                return;
            }
            catch (BackSelectedException e) {
                // Repeat
            }
        }
    }
}
