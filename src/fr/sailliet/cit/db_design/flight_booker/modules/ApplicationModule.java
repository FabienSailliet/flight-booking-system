package fr.sailliet.cit.db_design.flight_booker.modules;

import fr.sailliet.cit.db_design.flight_booker.model.DAOManager;

import java.security.InvalidParameterException;
import java.text.ParseException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;

/**
 * Part of the command line application.
 */
public abstract class ApplicationModule {

    private Scanner scanner;

    private DAOManager daoManager;
    private ApplicationModulesManager modulesManager;



    // Constructors

    /**
     * Construct a new application module.
     *
     * @param scanner Scanner from which all the input will be read
     * @param daoManager DAO manager to read and write the data from an to
     * @param modulesManager Application modules manager
     */
    public ApplicationModule(Scanner scanner, DAOManager daoManager, ApplicationModulesManager modulesManager) {
        this.scanner = scanner;
        this.daoManager = daoManager;
        this.modulesManager = modulesManager;
    }



    // Getters

    public DAOManager getDaoManager() {
        return this.daoManager;
    }

    public ApplicationModulesManager getModulesManager() {
        return this.modulesManager;
    }



    // Command line utility methods

    /**
     * Ask the user to enter a date in "DD/MM/YYYY" format.
     *
     * @param prompt Text displayed when asking to the user
     * @return the entered date
     * @throws BackSelectedException The user selected back
     */
    protected LocalDate dateInput(String prompt) throws BackSelectedException {
        while (true) {
            System.out.printf(
                "\n%s (DD/MM/YYYY, empty for back): ",
                prompt
            );

            try {
                String input = this.scanner.nextLine();
                if (input.equals("")) {
                    throw new BackSelectedException();
                }
                else {
                    return LocalDate.parse(input, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                }
            }
            catch (DateTimeParseException e) {
                System.out.println("Invalid input: invalid date format");
            }
        }
    }

    /**
     * Ask the user to enter a date and time in "DD/MM/YYYY HH:MM" format.
     *
     * @param prompt Text displayed when asking to the user
     * @return the entered date and time
     * @throws BackSelectedException The user selected back
     */
    protected LocalDateTime dateTimeInput(String prompt) throws BackSelectedException {
        while (true) {
            System.out.printf("\n%s (DD/MM/YYYY HH:MM, empty for back): ", prompt);

            try {
                String input = this.scanner.nextLine();
                if (input.equals("")) {
                    throw new BackSelectedException();
                }
                else {
                    return LocalDateTime.parse(input, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
                }
            }
            catch (DateTimeParseException e) {
                System.out.println("Invalid input: invalid format");
            }
        }
    }

    /**
     * Ask the user to enter a duration in "HH:MM" format.
     *
     * @param prompt Text displayed when asking to the user
     * @return the entered duration
     * @throws BackSelectedException The user selected back
     */
    protected Duration durationInput(String prompt) throws BackSelectedException {
        while (true) {
            System.out.printf("\n%s (HH:MM, empty for back): ", prompt);

            try {
                String input = this.scanner.nextLine();
                if (input.equals("")) {
                    throw new BackSelectedException();
                }
                else {
                    return this.parseDuration(input);
                }
            }
            catch (ParseException e) {
                System.out.println("Invalid input: invalid time format");
            }
        }
    }

    /**
     * Ask the user to enter a string. Cannot be empty because it is reserved to go back.
     *
     * @param prompt Text displayed when asking to the user
     * @return the entered string
     * @throws BackSelectedException The user selected back
     */
    protected String notEmptyStringInput(String prompt) throws BackSelectedException {
        System.out.printf(
            "\n%s (empty for back): ",
            prompt
        );

        String input = this.scanner.nextLine();

        if (input.equals("")) {
            throw new BackSelectedException();
        }
        else {
            return input;
        }
    }

    /**
     * Ask the user to enter a IANA string representation of a time zone (ex: Europe/Paris).
     *
     * @param prompt Text displayed when asking to the user
     * @return the entered time zone
     * @throws BackSelectedException The user selected back
     */
    protected ZoneId timeZoneInput(String prompt) throws BackSelectedException {
        while (true) {
            System.out.printf("\n%s (empty for back): ", prompt);

            try {
                String answer = this.scanner.nextLine();

                if (answer.equals("")) {
                    throw new BackSelectedException();
                }
                else {
                    return ZoneId.of(answer);
                }
            }
            catch (DateTimeException e) {
                System.out.println("Invalid input: invalid time zone");
            }
        }
    }

    /**
     * Ask the user to enter an integer between two inclusive bounds.
     *
     * @param prompt Text displayed when asking to the user
     * @param min Minimum expected value
     * @param max Maximum expected value
     * @return the entered value
     */
    protected int intInputInRange(String prompt, int min, int max) {
        while (true) {
            System.out.print(prompt + ": ");
            try {
                int answer = Integer.parseInt(this.scanner.nextLine());

                if (answer >= min && answer <= max) {
                    return answer;
                }
                else {
                    System.out.println(String.format(
                        "Invalid input: not in the range [%s, %s]",
                        min, max
                    ));
                }
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid input: not a number");
            }
        }
    }

    /**
     * Ask the user to enter a positive integer.
     *
     * @param prompt Text displayed when asking to the user
     * @return the entered integer
     * @throws BackSelectedException The user selected back
     */
    protected int positiveIntInput(String prompt) throws BackSelectedException {
        while (true) {
            System.out.printf("\n%s (empty for back): ", prompt);
            try {
                String input = this.scanner.nextLine();

                if (input.equals("")) {
                    throw new BackSelectedException();
                }

                int answer = Integer.parseInt(input);

                if (answer >= 0) {
                    return answer;
                }
                else {
                    System.out.println("Invalid input: must be positive");
                }
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid input: not a number");
            }
        }
    }

    /**
     * Ask the user to enter a double.
     *
     * @param prompt Text displayed when asking to the user
     * @return the entered double
     * @throws BackSelectedException The user selected back
     */
    protected double doubleInput(String prompt) throws BackSelectedException {
        while (true) {
            System.out.printf("\n%s (empty for back): ", prompt);

            try {
                String answer = this.scanner.nextLine();

                if (answer.equals("")) {
                    throw new BackSelectedException();
                }
                else {
                    return Double.parseDouble(answer);
                }
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid input: not a number");
            }
        }
    }

    /**
     * Ask confirmation to the user. Return normally if user confirmed.
     *
     * @param prompt Text displayed when asking to the user
     * @throws BackSelectedException The user didn't confirm
     */
    protected void askConfirmation(String prompt) throws BackSelectedException {
        System.out.print(String.format(
            "\n%s ([y]/n) ",
            prompt
        ));

        String answer = this.scanner.nextLine();

        if (!answer.equals("") && !answer.toUpperCase().equals("Y")) {
            throw new BackSelectedException();
        }
    }

    /**
     * Display a list of objects to the user and ask it to select one. Also displays at the end of the list a "Back" option
     *
     * @param prompt Text displayed to the user before the list
     * @param items List containing the items to display
     * @param toString Function called to get the string value to display for each item
     * @param <T> Type of the object to display
     * @return The selected item
     * @throws BackSelectedException Back has been selected
     */
    protected <T> T askSelectInList(String prompt, List<T> items, Function<T, String> toString) throws BackSelectedException {
        if (items.size() == 0) {
            throw new InvalidParameterException("The given list is empty");
        }

        System.out.println("\n" + prompt);

        // Display choices extracted from the given list

        int maxNbLength = ((int) Math.log10(items.size()+1)) + 1;

        StringBuilder newLineWhiteSpaceSequenceBuilder = new StringBuilder("\n\t");
        for (int i=0 ; i<maxNbLength+4 ; i++) {
            newLineWhiteSpaceSequenceBuilder.append(" ");
        }
        String newLineWhiteSpaceSequence = newLineWhiteSpaceSequenceBuilder.toString();

        int i = 1;
        for (T item: items) {
            System.out.println(String.format(
                "\t%-" + (maxNbLength + 1) + "s %s",
                i++ + ".", toString.apply(item).replace("\n", newLineWhiteSpaceSequence)
            ));
        }
        // Display back option
        System.out.println(String.format(
            "\t%-" + (maxNbLength + 1) + "s %s",
            i + ".", "Back"
        ));

        // Take user answer
        int answer = this.intInputInRange(
            "Please select one",
            1, items.size()+1
        );

        if (answer-1 >= items.size()) {
            System.out.println("Go back");
            throw new BackSelectedException();
        }
        else {
            T selected = items.get(answer - 1);
            System.out.println(String.format(
                "\nSelected \"%s\"",
                toString.apply(selected).split("\n")[0]
            ));
            return selected;
        }
    }

    /**
     * Display a header text with horizontal line.
     *
     * @param title Text to display in the line
     */
    protected void displayHeader(String title) {
        System.out.println("\n---------- " + title + " ----------");
    }

    /**
     * Display a simple horizontal line
     */
    protected void displayFooter() {
        System.out.println("\n------------------------------");
    }

    /**
     * Format a price to be displayed (remove useless .0 if needed and add the € symbol)
     *
     * @param price Price to display
     * @return Formatted price
     */
    protected String formatPrice(double price) {
        long lPrice = (long) price;

        if (price == lPrice) {
            return lPrice + "€";
        }
        else {
            return price + "€";
        }
    }

    /**
     * Format a date and time to be display, using the format "DD/MM/YYYY HH:MM".
     *
     * @param zonedDateTime Date and time to be formatted
     * @return Fornatted date and time
     */
    protected String formatDateTime(ZonedDateTime zonedDateTime) {
        return zonedDateTime.format(DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm"));
    }



    // Private methods

    private Duration parseDuration(String s) throws ParseException {
        String[] parts = s.split(":");
        if (parts.length != 2) {
            throw new ParseException("Cannot parse duration: must follow the format HH:MM", 0);
        }

        try {
            int hours = Integer.parseInt(parts[0]);
            int minutes = Integer.parseInt(parts[1]);

            return Duration.ofHours(hours).plusMinutes(minutes);

        } catch (NumberFormatException e) {
            throw new ParseException("Cannot parse duration: must follow the format HH:MM", 0);
        }
    }

}
