package fr.sailliet.cit.db_design.flight_booker.modules;

/**
 *  The user selected back
 */
public class BackSelectedException extends Exception {
    public BackSelectedException() {
    }

    public BackSelectedException(String s) {
        super(s);
    }

    public BackSelectedException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public BackSelectedException(Throwable throwable) {
        super(throwable);
    }

    public BackSelectedException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
