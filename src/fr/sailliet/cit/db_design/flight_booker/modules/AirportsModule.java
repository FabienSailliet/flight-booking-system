package fr.sailliet.cit.db_design.flight_booker.modules;

import fr.sailliet.cit.db_design.flight_booker.model.Airport;
import fr.sailliet.cit.db_design.flight_booker.model.DAOManager;

import java.time.ZoneId;
import java.util.Scanner;

public class AirportsModule extends ApplicationModule {

    // Constructors

    public AirportsModule(Scanner scanner, DAOManager daoManager, ApplicationModulesManager modulesManager) {
        super(scanner, daoManager, modulesManager);
    }



    // Public methods

    /**
     * Ask the user to select an airport, displaying all the airports with their code and name.
     *
     * @param prompt Text displayed before the airports list
     * @return Selected airport
     * @throws BackSelectedException The user selected back
     */
    public Airport selectAirport(String prompt) throws BackSelectedException {
        return this.askSelectInList(
            prompt,
            this.getDaoManager().getAirportDAO().getAllSortedByCode(),
            airport -> airport.getCode() + " - " + airport.getName()
        );
    }

    /**
     * Ask necessary information to the user, create a new airport and add it into the database.
     *
     * @throws BackSelectedException The user selected back
     */
    public void createAirport() throws BackSelectedException {
        while (true) {
            this.displayHeader("Airport creation");

            String code = this.notEmptyStringInput("Code");
            try {
                String name = this.notEmptyStringInput("Name");
                String city = this.notEmptyStringInput("City");
                String country = this.notEmptyStringInput("Country");
                ZoneId timeZone = this.timeZoneInput("Time zone");

                Airport airport = new Airport(code, name, city, country, timeZone.getId());

                this.getDaoManager().getAirportDAO().insert(airport);

                System.out.println("\nThe airport has been added");

                return;
            }
            catch (BackSelectedException e) {
                // Repeat
            }
        }
    }
}
