package fr.sailliet.cit.db_design.flight_booker.modules;

import fr.sailliet.cit.db_design.flight_booker.model.*;

import java.util.*;

public class MenusModule extends ApplicationModule {



    // Constructors

    public MenusModule(Scanner scanner, DAOManager daoManager, ApplicationModulesManager modulesManager) {
        super(scanner, daoManager, modulesManager);
    }



    // Public methods

    /**
     * Display the home application menu.
     */
    public void mainMenu() {
        while (true) {
            this.displayHeader("Main Menu");

            MenuElement selected;
            try {
                selected = this.askSelectInList(
                    "Available options",
                    Arrays.asList(
                        new MenuElement("Customer", this::customerMenu),
                        new MenuElement("Admin", this::adminMenu)
                    ),
                    MenuElement::getLabel
                );
            } catch (BackSelectedException e) {
                return;
            }

            try {
                selected.getCallback().run();
            } catch (BackSelectedException e) {
                // Repeat
            }
        }
    }

    /**
     * Display the customer actions menu.
     *
     * @throws BackSelectedException The user selected back
     */
    public void customerMenu() throws BackSelectedException {
        while (true) {
            this.displayHeader("Customer Menu");

            MenuElement selected = this.askSelectInList(
                "Available options",
                Arrays.asList(
                    new MenuElement("Search / book a flight",
                        this.getModulesManager().getFlightsModule()::searchAndBookAFlight
                    ),
                    new MenuElement("Manage bookings",
                        this.getModulesManager().getFlightsModule()::manageBookings
                    ),
                    (this.getModulesManager().getCustomerModule().isACustomerConnected()) ?
                        new MenuElement("Log out",
                            this.getModulesManager().getCustomerModule()::logOut
                        ) :
                        new MenuElement("Log in / Sign in",
                            this.getModulesManager().getCustomerModule()::logInOrSignIn
                        )
                ),
                MenuElement::getLabel
            );

            try {
                selected.getCallback().run();
            }
            catch (BackSelectedException e) {
                // Relaunch main menu
            }
        }
    }

    /**
     * Display the admin actions menu.
     *
     * @throws BackSelectedException The user selected back
     */
    public void adminMenu() throws BackSelectedException {
        this.getModulesManager().getAdminModule().logIn();

        while (true) {
            this.displayHeader("Administration Menu");

            MenuElement selected = this.askSelectInList(
                "Available options",
                Arrays.asList(
                    new MenuElement("Add airport", this.getModulesManager().getAirportsModule()::createAirport),
                    new MenuElement("Add flight", this.getModulesManager().getFlightsModule()::createFlight),
                    new MenuElement("Add plane", this.getModulesManager().getPlanesModule()::createPlane)
                ),
                MenuElement::getLabel
            );

            try {
                selected.getCallback().run();
            }
            catch (BackSelectedException e) {
                // Relaunch main menu
            }
        }
    }
}
