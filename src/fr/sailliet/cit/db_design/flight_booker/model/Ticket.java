package fr.sailliet.cit.db_design.flight_booker.model;

public class Ticket {
    private int id;
    private int customer;
    private int flight;
    private String passengerFirstName;
    private String passengerLastName;
    private int luggage;



    // Constructor

    public Ticket(int id, int customer, int flight, String passengerFirstName, String passengerLastName, int luggage) {
        this.id = id;
        this.customer = customer;
        this.flight = flight;
        this.passengerFirstName = passengerFirstName;
        this.passengerLastName = passengerLastName;
        this.luggage = luggage;
    }



    // Getters

    public int getId() {
        return this.id;
    }

    public int getCustomer() {
        return this.customer;
    }

    public int getFlight() {
        return this.flight;
    }

    public String getPassengerFirstName() {
        return this.passengerFirstName;
    }

    public String getPassengerLastName() {
        return this.passengerLastName;
    }

    public int getLuggage() {
        return this.luggage;
    }



    // Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setCustomer(int customer) {
        this.customer = customer;
    }

    public void setFlight(int flight) {
        this.flight = flight;
    }

    public void setPassengerFirstName(String passengerFirstName) {
        this.passengerFirstName = passengerFirstName;
    }

    public void setPassengerLastName(String passengerLastName) {
        this.passengerLastName = passengerLastName;
    }

    public void setLuggage(int luggage) {
        this.luggage = luggage;
    }



    // Other public methods

    @Override
    public String toString() {
        return String.format(
            "%s - %s %s",
            this.flight, this.passengerFirstName, this.passengerLastName
        );
    }
}
