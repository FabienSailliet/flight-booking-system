package fr.sailliet.cit.db_design.flight_booker.model;

/**
 * Raised when an error occurred during the model manipulation.
 */
public class ModelException extends Exception {
    public ModelException() {
    }

    public ModelException(String s) {
        super(s);
    }

    public ModelException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ModelException(Throwable throwable) {
        super(throwable);
    }

    public ModelException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
