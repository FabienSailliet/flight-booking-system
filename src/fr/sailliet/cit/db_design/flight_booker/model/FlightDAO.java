package fr.sailliet.cit.db_design.flight_booker.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

public class FlightDAO extends DAO<Flight> {

    // Constructors

    public FlightDAO(Connection connection) {
        super(connection);
    }



    // Requests methods

    /**
     * Get all the flights.
     *
     * @return A list containing all the flights
     */
    public List<Flight> getAll() {
        return this.getAll("SELECT * FROM Flight");
    }

    /**
     * Get a specific flight given its id.
     *
     * @param id Id of the desired flight
     * @return The desired flight object if found
     * @throws CannotFindDataException The given id doesn't belong to any registered flight
     */
    public Flight get(int id) throws CannotFindDataException {
        return this.getOne(
            "SELECT * FROM Flight WHERE id=?",
            statement -> {
                statement.setInt(1, id);
            }
        );
    }

    /**
     * Search for a flight between two airports around a specific date. Return all the flights found between this two airports departing between 7 days before and after the desired date.
     *
     * @param departureAirport Desired departure airport
     * @param arrivalAirport Desired arrival airport
     * @param departureDate Desired departure date
     * @return A list containing all the suitable flights
     */
    public List<Flight> findBetweenTwoAirports(
        Airport departureAirport,
        Airport arrivalAirport,
        LocalDate departureDate
    ) {
        return this.getAll(
            "SELECT * FROM Flight " +
                "WHERE departure_airport=? AND arrival_airport=? " +
                "AND ABS(DATEDIFF(departure_time, ?)) <= 7 " +
                "ORDER BY departure_time",
            statement -> {
                statement.setString(1, departureAirport.getCode());
                statement.setString(2, arrivalAirport.getCode());
                statement.setString(3, departureDate.toString());
            }
        );
    }

    /**
     * Add a new flight to the database, automatically choosing the id. This method will not consider what {@link Flight#getId()} returns.
     *
     * @param flight Flight to add
     */
    public void insertWithAutoIncrement(Flight flight) {
        this.execute(
            "INSERT INTO Flight(departure_time, arrival_time, departure_airport, arrival_airport, plane, price) " +
                "VALUES (?, ?, ?, ?, ?, ?)",
            statement -> {
                statement.setObject(1,
                    flight.getDepartureTime().withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime()
                );
                statement.setObject(2,
                    flight.getArrivalTime().withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime()
                );
                statement.setString(3, flight.getDepartureAirport());
                statement.setString(4, flight.getArrivalAirport());
                statement.setInt(5, flight.getPlane());
                statement.setDouble(6, flight.getPrice());
            }
        );
    }



    // Requests results processing methods

    @Override
    protected Flight getOneFromResultSet(ResultSet results) throws SQLException {
        return new Flight(
            results.getInt("Flight.id"),
            results.getTimestamp("Flight.departure_time")
                .toLocalDateTime().atZone(ZoneId.of("UTC")),
            results.getTimestamp("Flight.arrival_time")
                .toLocalDateTime().atZone(ZoneId.of("UTC")),
            results.getString("Flight.departure_airport"),
            results.getString("Flight.arrival_airport"),
            results.getInt("Flight.plane"),
            results.getDouble("Flight.price")
        );
    }
}
