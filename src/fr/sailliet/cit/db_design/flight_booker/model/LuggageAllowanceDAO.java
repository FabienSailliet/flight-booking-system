package fr.sailliet.cit.db_design.flight_booker.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class LuggageAllowanceDAO extends DAO<LuggageAllowance> {

    // Constructors

    public LuggageAllowanceDAO(Connection connection) {
        super(connection);
    }



    // Requests methods

    /**
     * Get all the luggage allowances.
     *
     * @return A list containing all the luggage allowances
     */
    public List<LuggageAllowance> getAll() {
        return this.getAll("SELECT * FROM LuggageAllowance");
    }

    /**
     * Get all the luggage allowances sorted by their weight, lower first.
     *
     * @return A list containing all the luggage allowances sorted by their weight
     */
    public List<LuggageAllowance> getAllSortedByWeight() {
        return this.getAll("SELECT * FROM LuggageAllowance ORDER BY weight");
    }

    /**
     * Get a specific luggage allowance given its weight.
     *
     * @param weight Weight of the desired luggage allowance
     * @return The desired luggage allowance object if found
     * @throws CannotFindDataException The given weight doesn't belong to any registered luggage allowance
     */
    public LuggageAllowance get(int weight) throws CannotFindDataException {
        return this.getOne(
            "SELECT * FROM LuggageAllowance WHERE weight=?",
            statement -> {
                statement.setInt(1, weight);
            }
        );
    }



    // Requests results processing methods

    @Override
    protected LuggageAllowance getOneFromResultSet(ResultSet results) throws SQLException {
        return new LuggageAllowance(
            results.getInt("LuggageAllowance.weight"),
            results.getDouble("LuggageAllowance.price_ratio")
        );
    }
}
