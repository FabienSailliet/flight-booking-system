package fr.sailliet.cit.db_design.flight_booker.model;

public class Admin {

    private String login;
    private String password;



    // Constructors

    public Admin(String login, String password) {
        this.login = login;
        this.password = password;
    }



    // Getters

    public String getLogin() {
        return this.login;
    }

    public String getPassword() {
        return this.password;
    }



    // Setters

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
