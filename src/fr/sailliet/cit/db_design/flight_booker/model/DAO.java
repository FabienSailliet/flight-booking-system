package fr.sailliet.cit.db_design.flight_booker.model;

import org.intellij.lang.annotations.Language;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * A class used to get and put data from and into the database.
 *
 * @param <T> Type used by an implementation of this class. This type should reflect a database's table.
 */
public abstract class DAO<T> {

    private Connection connection;



    // Constructors

    /**
     * Construct a new DAO.
     *
     * @param connection SQL connection that will be used by this DAO
     */
    public DAO(Connection connection) {
        this.connection = connection;
    }



    // Requests methods

    /**
     * Run an SQL query and return all the result's rows, parsed with {@link #getOneFromResultSet(ResultSet) getOneFromResultSet()} method. Should only be used for select requests.
     *
     * @param query SQL query to run
     * @return List containing all the objects got
     */
    protected List<T> getAll(@Language("SQL")String query) {
        try {

            try (
                Statement statement = this.connection.createStatement();
                ResultSet results = statement.executeQuery(query)
            ) {
                return this.getAllFromResults(results);
            }

        } catch (SQLException e) {
            throw new RuntimeException("Cannot get table rows", e);
        }
    }

    /**
     * Run a parametrized SQL query and return all the result's rows, parsed with {@link #getOneFromResultSet(ResultSet) getOneFromResultSet()} method. Should only be used for select requests.
     *
     * @param query SQL query to run, should contain one question mark at each place a value has to be inserted
     * @param statementFiller Method to call to fill the prepared statement
     * @return List containing all the objects got
     */
    protected List<T> getAll(@Language("SQL")String query, PreparedStatementFiller statementFiller) {
        try (PreparedStatement statement = this.connection.prepareStatement(query)) {
            statementFiller.fill(statement);

            try (ResultSet results = statement.executeQuery()) {
                return this.getAllFromResults(results);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get table rows", e);
        }
    }

    /**
     * Run a parametrized SQL query and return the first result's row, parsed with {@link #getOneFromResultSet(ResultSet) getOneFromResultSet()} method. Should only be used for select requests.
     *
     * @param query SQL query to run, should contain one question mark at each place a value has to be inserted
     * @param statementFiller Method to call to fill the prepared statement
     * @return List containing all the objects got
     */
    protected T getOne(@Language("SQL")String query, PreparedStatementFiller statementFiller) throws CannotFindDataException {
        try (PreparedStatement statement = this.connection.prepareStatement(query)) {
            statementFiller.fill(statement);

            try (ResultSet results = statement.executeQuery()) {
                if (results.next()) {
                    return this.getOneFromResultSet(results);
                } else {
                    throw new CannotFindDataException("Request returns empty set");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot get table row", e);
        }
    }

    /**
     * Execute an SQL query without getting the result. Should be used for update and delete requests.
     *
     * @param query SQL query to run, should contain one question mark at each place a value has to be inserted
     * @param statementFiller Method to call to fill the prepared statement
     */
    protected void execute(@Language("SQL")String query, PreparedStatementFiller statementFiller) {
        try (PreparedStatement statement = this.connection.prepareStatement(query)) {

            statementFiller.fill(statement);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Cannot execute request", e);
        }
    }



    // Requests results processing methods

    private List<T> getAllFromResults(ResultSet results) throws SQLException {
        List<T> ret = new LinkedList<>();

        while (results.next()) {
            ret.add(this.getOneFromResultSet(results));
        }

        return ret;
    }

    /**
     * Parse an object from an SQL {@link ResultSet}. Used by the select method to get one object for each request's result line. As the order of the columns cannot be guaranteed, the data should be get with the column's name, and not number.
     *
     * @param results {@link ResultSet} from which the object's data must be read
     * @return The newly created object
     * @throws SQLException An error occurred while trying to get a line's data
     */
    protected abstract T getOneFromResultSet(ResultSet results) throws SQLException;
}
