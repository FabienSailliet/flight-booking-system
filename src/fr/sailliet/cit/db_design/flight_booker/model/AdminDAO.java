package fr.sailliet.cit.db_design.flight_booker.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AdminDAO extends DAO<Admin> {

    // Constructors

    public AdminDAO(Connection connection) {
        super(connection);
    }



    // Requests methods

    /**
     * Get all the admins.
     *
     * @return A list containing all the admins
     */
    public List<Admin> getAll() {
        return this.getAll("SELECT * FROM Admin");
    }

    /**
     * Get a specific admin given its login.
     *
     * @param login Login of the desired admin
     * @return The desired admin object if found
     * @throws CannotFindDataException The given login doesn't belong to any registered admin
     */
    public Admin get(String login) throws CannotFindDataException {
        return this.getOne(
            "SELECT * FROM Admin WHERE login=?",
            statement -> {
                statement.setString(1, login);
            }
        );
    }



    // Requests results processing methods

    @Override
    protected Admin getOneFromResultSet(ResultSet results) throws SQLException {
        return new Admin(
            results.getString("Admin.login"),
            results.getString("Admin.password")
        );
    }
}
