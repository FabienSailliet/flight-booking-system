package fr.sailliet.cit.db_design.flight_booker.model;

import java.sql.Connection;

public class DAOManager {

    private AirportDAO airportDAO;
    private CustomerDAO customerDAO;
    private FlightDAO flightDAO;
    private LuggageAllowanceDAO luggageAllowanceDAO;
    private PlaneDAO planeDAO;
    private TicketDAO ticketDAO;
    private AdminDAO adminDAO;



    // Constructors

    public DAOManager(Connection connection) {
        this.airportDAO = new AirportDAO(connection);
        this.customerDAO = new CustomerDAO(connection);
        this.flightDAO = new FlightDAO(connection);
        this.luggageAllowanceDAO = new LuggageAllowanceDAO(connection);
        this.planeDAO = new PlaneDAO(connection);
        this.ticketDAO = new TicketDAO(connection);
        this.adminDAO = new AdminDAO(connection);
    }



    // Getters

    public AirportDAO getAirportDAO() {
        return this.airportDAO;
    }

    public CustomerDAO getCustomerDAO() {
        return this.customerDAO;
    }

    public FlightDAO getFlightDAO() {
        return this.flightDAO;
    }

    public LuggageAllowanceDAO getLuggageAllowanceDAO() {
        return this.luggageAllowanceDAO;
    }

    public PlaneDAO getPlaneDAO() {
        return this.planeDAO;
    }

    public TicketDAO getTicketDAO() {
        return this.ticketDAO;
    }

    public AdminDAO getAdminDAO() {
        return this.adminDAO;
    }
}
