package fr.sailliet.cit.db_design.flight_booker.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AirportDAO extends DAO<Airport> {

    // Constructors

    public AirportDAO(Connection connection) {
        super(connection);
    }



    // Requests methods

    /**
     * Get all the airports.
     *
     * @return A list containing all the airports
     */
    public List<Airport> getAll() {
        return this.getAll("SELECT * FROM Airport");
    }

    /**
     * Get all the airports sorted by their code, lower first.
     *
     * @return A list containing all the airports sorted by their code
     */
    public List<Airport> getAllSortedByCode() {
        return this.getAll("SELECT * FROM Airport ORDER BY code");
    }

    /**
     * Get a specific airport given its code.
     *
     * @param code Code of the desired airport
     * @return The desired airport object if found
     * @throws CannotFindDataException The given code doesn't belong to any registered airport
     */
    public Airport get(String code) throws CannotFindDataException {
        return this.getOne(
            "SELECT * FROM Airport WHERE code=?",
            statement -> {
                statement.setString(1, code);
            }
        );
    }

    /**
     * Add a new airport to the database.
     *
     * @param airport Airport to add
     */
    public void insert(Airport airport) {
        this.execute(
            "INSERT INTO Airport(code, name, city, country, time_zone) " +
                "VALUES (?, ?, ?, ?, ?)",
            statement -> {
                statement.setString(1, airport.getCode());
                statement.setString(2, airport.getName());
                statement.setString(3, airport.getCity());
                statement.setString(4, airport.getCountry());
                statement.setString(5, airport.getTimeZone());
            }
        );
    }



    // Requests results processing methods

    @Override
    protected Airport getOneFromResultSet(ResultSet results) throws SQLException {
        return new Airport(
            results.getString("Airport.code"),
            results.getString("Airport.name"),
            results.getString("Airport.city"),
            results.getString("Airport.country"),
            results.getString("Airport.time_zone")
        );
    }
}
