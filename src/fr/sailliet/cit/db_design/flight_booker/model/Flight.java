package fr.sailliet.cit.db_design.flight_booker.model;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Flight {
    private int id;
    private ZonedDateTime departureTime;
    private ZonedDateTime arrivalTime;
    private String departureAirport;
    private String arrivalAirport;
    private int Plane;
    private double Price;



    // Constructors

    public Flight(int id, ZonedDateTime departureTime, ZonedDateTime arrivalTime, String departureAirport, String arrivalAirport, int plane, double price) {
        this.id = id;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.Plane = plane;
        this.Price = price;
    }



    // Getters

    public int getId() {
        return this.id;
    }

    public ZonedDateTime getDepartureTime() {
        return this.departureTime;
    }

    public ZonedDateTime getArrivalTime() {
        return this.arrivalTime;
    }

    public String getDepartureAirport() {
        return this.departureAirport;
    }

    public String getArrivalAirport() {
        return this.arrivalAirport;
    }

    public int getPlane() {
        return this.Plane;
    }

    public double getPrice() {
        return this.Price;
    }



    // Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setDepartureTime(ZonedDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public void setArrivalTime(ZonedDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public void setPlane(int plane) {
        this.Plane = plane;
    }

    public void setPrice(double price) {
        this.Price = price;
    }



    // Other public methods

    @Override
    public String toString() {
        return String.format(
            "%s %s - %s %s",
            this.departureAirport, this.departureTime.format(DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm")),
            this.arrivalAirport, this.arrivalTime.format(DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm"))
        );
    }
}
