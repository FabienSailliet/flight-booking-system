package fr.sailliet.cit.db_design.flight_booker.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class TicketDAO extends DAO<Ticket> {
    public TicketDAO(Connection connection) {
        super(connection);
    }



    // Requests methods

    /**
     * Get all the tickets.
     *
     * @return A list containing all the tickets
     */
    public List<Ticket> getAll() {
        return this.getAll("SELECT * FROM Ticket");
    }

    /**
     * Get a specific ticket given its id.
     *
     * @param id Id of the desired ticket
     * @return The desired ticket object if found
     * @throws CannotFindDataException The given id doesn't belong to any registered ticket
     */
    public Ticket get(int id) throws CannotFindDataException {
        return this.getOne(
            "SELECT * FROM Ticket WHERE id=?",
            statement -> {
                statement.setInt(1, id);
            }
        );
    }

    /**
     * Get all the tickets booked by a given customer.
     *
     * @param customer Customer we want the tickets of
     * @return A list containing the tickets booked by this customer
     */
    public List<Ticket> getAllByCustomer(Customer customer) {
        return this.getAll(
            "SELECT * " +
                "FROM Ticket JOIN Flight on Ticket.flight = Flight.id " +
                "WHERE customer=? " +
                "ORDER BY departure_time",
            statement -> {
                statement.setInt(1, customer.getId());
            }
        );
    }

    /**
     * Add a new ticket to the database, automatically choosing the id. This method will not consider what {@link Ticket#getId()} returns.
     *
     * @param ticket Ticket to add
     */
    public void insertWithAutoIncrement(Ticket ticket) {
        this.execute(
            "INSERT INTO Ticket(customer, flight, passenger_first_name, passenger_last_name, luggage) " +
                "VALUES (?, ?, ?, ?, ?)",
            statement -> {
                statement.setInt(1, ticket.getCustomer());
                statement.setInt(2, ticket.getFlight());
                statement.setString(3, ticket.getPassengerFirstName());
                statement.setString(4, ticket.getPassengerLastName());
                statement.setInt(5, ticket.getLuggage());
            }
        );
    }

    /**
     * Delete a given ticket from the database.
     *
     * @param ticket Ticket to delete
     */
    public void delete(Ticket ticket) {
        this.execute(
            "DELETE FROM Ticket WHERE id=?",
            statement -> {
                statement.setInt(1, ticket.getId());
            }
        );
    }



    // Requests results processing methods

    @Override
    protected Ticket getOneFromResultSet(ResultSet results) throws SQLException {
        return new Ticket(
            results.getInt("Ticket.id"),
            results.getInt("Ticket.customer"),
            results.getInt("Ticket.flight"),
            results.getString("Ticket.passenger_first_name"),
            results.getString("Ticket.passenger_last_name"),
            results.getInt("Ticket.luggage")
        );
    }
}
