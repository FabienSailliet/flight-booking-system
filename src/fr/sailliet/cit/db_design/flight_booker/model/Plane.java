package fr.sailliet.cit.db_design.flight_booker.model;

public class Plane {
    private int id;
    private String model;
    private int seatsNb;



    // Constructors

    public Plane(int id, String model, int seatsNb) {
        this.id = id;
        this.model = model;
        this.seatsNb = seatsNb;
    }



    // Getters

    public int getId() {
        return this.id;
    }

    public String getModel() {
        return this.model;
    }

    public int getSeatsNb() {
        return this.seatsNb;
    }



    // Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setSeatsNb(int seatsNb) {
        this.seatsNb = seatsNb;
    }



    // Other methods

    @Override
    public String toString() {
        return this.model;
    }
}
