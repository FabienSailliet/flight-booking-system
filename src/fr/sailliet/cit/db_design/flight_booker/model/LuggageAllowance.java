package fr.sailliet.cit.db_design.flight_booker.model;

public class LuggageAllowance {
    private int weight;
    private double priceRatio;



    // Constructors

    public LuggageAllowance(int weight, double priceRatio) {
        this.weight = weight;
        this.priceRatio = priceRatio;
    }



    // Getters

    public int getWeight() {
        return this.weight;
    }

    public double getPriceRatio() {
        return this.priceRatio;
    }



    // Setters

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setPriceRatio(double priceRatio) {
        this.priceRatio = priceRatio;
    }



    // Other public methods

    @Override
    public String toString() {
        return this.weight + "kg";
    }
}
