package fr.sailliet.cit.db_design.flight_booker.model;

import java.sql.*;
import java.util.List;

public class CustomerDAO extends DAO<Customer> {

    // Constructors

    public CustomerDAO(Connection connection) {
        super(connection);
    }



    // Public methods

    /**
     * Get all the customers.
     *
     * @return A list containing all the customers
     */
    public List<Customer> getAll() {
        return this.getAll("SELECT * FROM Customer");
    }

    /**
     * Get a specific customer given its code.
     *
     * @param code Code of the desired customer
     * @return The desired customer object if found
     * @throws CannotFindDataException The given code doesn't belong to any registered customer
     */
    public Customer get(int code) throws CannotFindDataException {
        return this.getOne(
            "SELECT * FROM Customer WHERE id=?",
            statement -> {
                statement.setInt(1, code);
            }
        );
    }

    /**
     * Get a specific customer given its email.
     *
     * @param email Email of the desired customer
     * @return The desired customer object if found
     * @throws CannotFindDataException The given email doesn't belong to any registered customer
     */
    public Customer getByEmail(String email) throws CannotFindDataException {
        return this.getOne(
            "SELECT * FROM Customer WHERE email=?",
            statement -> {
                statement.setString(1, email);
            }
        );
    }

    /**
     * Add a new customer to the database, automatically choosing the code. This method will not consider what {@link Customer#getId()} returns.
     *
     * @param customer Customer to add
     */
    public void insertWithAutoIncrement(Customer customer) {
        this.execute(
            "INSERT INTO Customer(email, password, first_name, last_name) " +
                "VALUE (?, ?, ?, ?)",
            statement -> {
                statement.setString(1, customer.getEmail());
                statement.setString(2, customer.getPassword());
                statement.setString(3, customer.getFirstName());
                statement.setString(4, customer.getLastName());
            }
        );
    }

    /**
     * Check if an email is free or already taken by an existing user.
     *
     * @param email Email to check
     * @return True if the email is free, false if already taken
     */
    public boolean isEmailFree(String email) {
        try {
            this.getByEmail(email);
            return false;
        }
        catch (CannotFindDataException e) {
            return true;
        }
    }



    // Private methods

    @Override
    protected Customer getOneFromResultSet(ResultSet results) throws SQLException {
        return new Customer(
            results.getInt("Customer.id"),
            results.getString("Customer.email"),
            results.getString("Customer.password"),
            results.getString("Customer.first_name"),
            results.getString("Customer.last_name")
        );
    }
}
