package fr.sailliet.cit.db_design.flight_booker.model;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Airport {
    private String code;
    private String name;
    private String city;
    private String country;
    private String timeZone;



    // Constructors

    public Airport(String code, String name, String city, String country, String timeZone) {
        this.code = code;
        this.name = name;
        this.city = city;
        this.country = country;
        this.timeZone = timeZone;
    }



    // Getters

    public String getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }

    public String getCity() {
        return this.city;
    }

    public String getCountry() {
        return this.country;
    }

    public String getTimeZone() {
        return this.timeZone;
    }



    // Setters

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }


    // Other public methods

    public ZonedDateTime getLocalTime(ZonedDateTime zonedDateTime) {
        return zonedDateTime.withZoneSameInstant(ZoneId.of(this.getTimeZone()));
    }

    @Override
    public String toString() {
        return this.name;
    }
}
