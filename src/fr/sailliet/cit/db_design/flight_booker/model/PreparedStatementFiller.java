package fr.sailliet.cit.db_design.flight_booker.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Object used to fill a {@link PreparedStatement} with some data.
 */
public interface PreparedStatementFiller {
    /**
     * Fill a statement with data.
     *
     * @param statement Prepared statement to fill
     * @throws SQLException An SQL exception occurred while trying to fill the prepared statement
     */
    void fill(PreparedStatement statement) throws SQLException;
}
