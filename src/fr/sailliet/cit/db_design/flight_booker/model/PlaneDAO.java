package fr.sailliet.cit.db_design.flight_booker.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PlaneDAO extends DAO<Plane> {
    public PlaneDAO(Connection connection) {
        super(connection);
    }



    // Requests methods

    /**
     * Get all the planes.
     *
     * @return A list containing all the planes
     */
    public List<Plane> getAll() {
        return this.getAll("SELECT * FROM Plane");
    }

    /**
     * Get a specific plane given its id.
     *
     * @param id Id of the desired plane
     * @return The desired plane object if found
     * @throws CannotFindDataException The given id doesn't belong to any registered plane
     */
    public Plane get(int id) throws CannotFindDataException {
        return this.getOne(
            "SELECT * FROM Plane WHERE id=?",
            statement -> {
                statement.setInt(1, id);
            }
        );
    }

    /**
     * Add a new plane to the database, automatically choosing the id. This method will not consider what {@link Plane#getId()} returns.
     *
     * @param plane Plane to add
     */
    public void insertWithAutoIncrement(Plane plane) {
        this.execute(
            "INSERT INTO Plane(model, seats_nb) VALUES (?, ?)",
            statement -> {
                statement.setString(1, plane.getModel());
                statement.setInt(2, plane.getSeatsNb());
            }
        );
    }



    // Requests results processing methods

    @Override
    protected Plane getOneFromResultSet(ResultSet results) throws SQLException {
        return new Plane(
            results.getInt("Plane.id"),
            results.getString("Plane.model"),
            results.getInt("Plane.seats_nb")
        );
    }
}
