package fr.sailliet.cit.db_design.flight_booker.model;

/**
 * Raised when tried to get an entity that doesn't exist (ex: nonexistent primary key)
 */
public class CannotFindDataException extends ModelException {
    public CannotFindDataException() {
    }

    public CannotFindDataException(String s) {
        super(s);
    }

    public CannotFindDataException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public CannotFindDataException(Throwable throwable) {
        super(throwable);
    }

    public CannotFindDataException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
