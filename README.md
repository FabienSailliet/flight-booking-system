# Flight Booking System

This Java application manages flight bookings, allowing customers to book some tickets for a flight, and administrators to add data to the database, as new airports, planes and flights.

It is a Java command line application, using the JDBC library to connect to a MySQL database.

[report.pdf](/uploads/c98c5135f46e8c40dd5baf3639a3a5f3/report.pdf)

## Important

To adapt this project to a database, it's necessary to change the following line in the class `ConnectionFactory` in the package `fr.sailliet.cit.db_design.flight_booker.model`:

```java
private static final String URL = "jdbc:mysql://[host]:3306/[database_name]";
private static final String USER = "user";
private static final String PASS = "password";
```