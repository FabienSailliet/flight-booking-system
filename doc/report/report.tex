\documentclass[oneside]{report}

\input{framework.tex}

\begin{document}

\title{
    Flight Booking System\\
    Final Report
}
\author{
    Fabien Sailliet\\
    R00184006\\
    SDH-2\\
    \\
    Cork Institute of Technology\\
    Cork, Ireland
}
\maketitle

\thispagestyle{empty}

\chapter*{Sign Off of Own Work}
    \setcounter{page}{0}
    \thispagestyle{empty}

    I hereby certify that this material which I now submit for assessment, is entirely my own work and has not been taken from the work of others, save and to the extent, that such work has been cited and acknowledged within the text of my work.

% Remove appendices from main table of contents
\etocdepthtag.toc{main}
\etocsettagdepth{main}{section}
\etocsettagdepth{appendices}{none}
\tableofcontents

\chapter{Project Description}

    This project is a flight bookings managing system, allowing customers to buy tickets for flights managed by administrators. This application is separated in two parts: the customers and the administrators section.

    In the first section, a customer can search for a flight, given departure and arrival airports and the desired departure date. They can also see their registered bookings and cancel them. To allow that, they have to log in, so they can also create a new customer account.

    In the second section, that can only be accessed by an administrator thanks to a login and a password, they can register new airports, planes and flights.

    This application is accessible through a command line client, and stores its data in a MySQL database.

\chapter{Technical Issues}
    \label{ch:Technical Issues}

    The main issue I had was to represent in the Java program the flights' departure and arrival date and time, which are stored in the database with the MySQL data type \code{DATETIME}. Looking at the \code{java.sql.ResultSet} documentation\cite{javaSqlDoc}, I was not able to find any \code{getDateTime()} method. The only methods I found were \code{getDate()} and \code{getTime()}, which return objects from the \code{java.sql} package mainly marked as deprecated. After some research, I finally used the \code{java.time} package\cite{javaTimeDoc}. I can get the date and time by calling \code{resultSet.getTimestamp().toLocalDateTime()}, which return an instance of \code{java.time.LocalDateTime}. To write it back into the database, I use the \code{setString()} method with the \code{LocalDateTime} ISO representation.

\chapter{Database Design}

    In this part, we will see how the data is organised in the MySQL database. You can find in the appendices the entity relationship diagram\footnote{
        See appendix \vref{app:erd}
    } and the database creation script\footnote{
        See appendix \vref{app:Database creation script}
    }, and we will see in details the tables' content in the next sections.

    \section{Admin Table}

        The \code{Admin} table contains the login information of the administrators, allowning for example to add new airports and planes to the database.

        \begin{tabledescription}
            \item[login] login identifier 
            \item[password] password, clear text, not hashed
        \end{tabledescription}

    \section{Airport Table}

        The \code{Airport} table contains various informations used by the user, such as the city and country, along with the time zone, which is used by the system to display local times.

        \begin{tabledescription}
            \item[code] IATA three-letter airport code
            \item[name] official airport name
            \item[city] name of the city in which the airport is
            \item[country] name of the country in which the airport is
            \item[time\_zone] IANA time zone string representation\cite{ianaTimeZones}
        \end{tabledescription}

    \section{Plane Table}

        The \code{Plane} table contains informations about each individual plane that can be used for a flight.

        \begin{tabledescription}
            \item[id] identifier of the plane, auto-incremented
            \item[model] model name of the plane
            \item[seats\_nb] number of passengers a plane can carry
        \end{tabledescription}

    \section{Flight Table}

        The \code{Flight} table contains information about each one-time flight.

        \begin{tabledescription}
            \item[id] identifier of the flight, auto-incremented
            \item[departure\_time] date and time at which the flight begins, at UTC
            \item[arrival\_time] date and time at which the flight ends, at UTC
            \item[departure\_airport] code of the airport where the flight begins
            \item[arrival\_airport] code of the airport where the flight ends
            \item[plane] id of the plane used for this flight
            \item[price] base price of the flight, in euros
        \end{tabledescription}

    \section{Customer Table}

        The \code{Customer} table contains information about the customers that have sign in, such as login information.

        \begin{tabledescription}
            \item[id] identifier of the customer, auto-incremented
            \item[email] email address indicated by the user, unique among all the users
            \item[password] password, clear text, not hashed
            \item[first\_name] customer first name
            \item[last\_name] customer last name
        \end{tabledescription}

    \section{LuggageAllowance Table}

        The \code{LuggageAllowance} table contains information about the different luggage options offered for the flights.

        \begin{tabledescription}
            \item[weight] weight that a passenger is allowed to carry if they take this option
            \item[price\_ratio] price of the option, relative to the flight's base price
        \end{tabledescription}

    \section{Ticket Table}

        The \code{Ticket} table contains information about the customers' bookings.

        \begin{tabledescription}
            \item[id] identifier of the booking, auto-incremented
            \item[customer] identifier of the customer who bought this booking
            \item[flight] identifier of the flight this booking is for
            \item[passenger\_first\_name] first name of the person who will use this booking
            \item[passenger\_last\_name] last name of the person who will use this booking
            \item[luggage] weight of the luggage allowance option associated with this booking
        \end{tabledescription}

\chapter{Application Design}

    A real MVC\footnote{
        Model-View-Controller: design pattern that can be used to organise a graphical application. It is composed of three distinct parts: the Model which is the data manipulated by the program, the View which manages all the interactions with the user, and the Controller which makes the link between the two.
    } being complicated to use in a command-line application, I decided to adapt it into a simpler form. This way, I kept the Model part which consists of some classes abstracting the access to the database, and I merged the view and the controller into a single View-Controller part. In this part, we will see in detail how these two parts are organised.

    \section{Model}

        In this part, we will first quickly present the design pattern that has been used, and then detail the actual implementation in this project.

        \subsection{DAO Design Pattern}

            In order to organise the access to the database in a flexible way, I used the DAO design pattern\cite{daoDesignPattern}, in its simplest version. It allows to abstract the SQL requests in dedicated DAO classes, which return and take Java objects representing a database table. A sequence diagram of this design pattern can be found in figure \vref{fig:seq_diag}.

            \begin{figure}[h]
                \centering
                \includegraphics[height=10cm]{img/dao_sequence_diagram.png}
                \caption{DAO Design Pattern Sequence Diagram}
                \label{fig:seq_diag}
            \end{figure}

        \subsection{Implementation in the Project}

            In order to make it easier to explain how this design pattern has been implemented in the project, a class diagram of some of the involved classes can be found in appendix \vref{app:DAO class diagram}.

            By following this design pattern, I made two classes for each database table. The first has the same name than the table, and simply reflects its columns. It has one private field, one getter and one setter for each column, and can have some other methods to help getting complicated data, such as a local time, or a displayable string. The second class has this same name followed by \code{DAO}. It contains all the methods necessary to get data from the table and to insert new one. For example, the most common method I implemented in all these classes is \code{getAll()}, which returns a list of objects representing all the lines of the corresponding table.

            To help me implementing this data manipulation methods, I made all the DAO classes inherit an abstract \code{DAO} class\footnote{
                The code of the \code{DAO} class can be found in appendix \vref{app:DAO class code}
            }. The latter defines methods that take the SQL query and return a list of instantiated objects corresponding to the request's results. These help a lot to write the specific DAO classes. When a query needs variable parameters, it is possible to give it a \code{PreparedStatementFiller}, which defines a methods that will be called after the prepared statement has been created, in order to fill it with necessary data. An exemple of this can be found in the \code{TicketDAO} class\footnote{
                The code of the \code{TicketDAO} class can be found in appendix \vref{app:TicketDAO class code}
            }

            Finally, the \code{DAO} class defines the abstract method \code{getOneFromResultSet()}. This method must be implemented in every child DAO class to read the necessary data from the current \code{ResultSet}'s line in order to create and return a data object representing it.

    \section{View-Controller}

        We will now see the View-Controller part, whose a part of the class diagram can be found in the appendix \vref{app:VS class diagram}. This part is made up of code to display informations to the user, ask them some and communicate them to the Model. It is divided in multiple classes, which represent features module. For example, the \code{PlanesModule} class allows to ask the user to select a registered plane (displays a list of all the planes and wait for the user to select one) or to ask him some informations to create a new one before adding it to the database\footnote{
            The code of the \code{PlanesModules} class can be found in appendix \vref{app:PlanesModule class code}
        }.

        Each module can access to the others through the \code{ApplicationModulesManager} class. Every module class inherits from the abstract \code{ApplicationModule} class, which defines some methods to display information to the user and ask them some (for example, the most used method, \code{askSelectInList()}, displays a list of objects to the user and ask them to select one).

\chapter{Project Management}

    Following a project advancement being important for its success, I did so by two means: an activity log\footnote{
        The activity log can be found in appendix \vref{app:activity journal}
    } and a Gantt diagram\footnote{
        The Gantt diagram can be found in appendix \vref{app:Gantt diagram}
    }.

    In the activity log, I added after each work day a summary of what I did, with links to important ressources I used. When I had some issues about something, I explained briefly this issue so that I can more easily come back to it later if I do not solve it in one day.

    At the start of the project, I created a Gantt diagram where I put all the tasks I had to do. Regularly, I updated it to increase the length of tasks that were longer than expected, or add new unexpected ones. That way, I was aware of where I was in the project and if I would be able to get it finished in time.

    As we can see in the Gantt diagram, I spent to much time on the initial database design. By doing so, I did not have a lot of remaining time to develop the client application, which was the longer part. Also, for the latter, I did not have any safety margin, so the issue I had with the \code{DATETIME} SQL data type manipulation in Java\footnote{
        See part \vref{ch:Technical Issues}
    } moved all the other tasks to the last week, which makes the project difficult to finish.

\chapter*{Conclusion}
    \addcontentsline{toc}{chapter}{Conclusion}

    This project was the first time I used Java to create a database client application. I discovered the DAO design pattern and learnt how to use the JDBC API, which now allows me to easily develop any database access Java application.

    Some features remain to be implemented in this application, such as administrator ability to delete data from the tables or checking if a flight is full before accepting the customer booking, but thanks to the framework I created this features would not be long to add.

\printbibliography
\addcontentsline{toc}{chapter}{Bibliography}

\input{appendices.tex}
    
\end{document}
